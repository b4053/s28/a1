//3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {	console.log(data)
})


//4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {	
	let todoList = data.map((todos => {
		return todos.title;
	}))
	console.log(todoList);
})


//5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())


//6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

.then(data => {	console.log(`Todo list title:${data.title}, status:${data.completed}`)
})


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New To Do List',
		body: 'Created new to do list.',
		userId: 1
	})  
})
.then(response => response.json())
.then(data => console.log(data)) 


// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated To Do List",
		body: "This is updated.",
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))


// 9. Update a to do list item by changing the data structure to contain the following properties:
// - Title
// - Description
// - Status
// - Date Completed
// - User ID

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updating To Do List Item",
		dDescription: "Updated To Do List Item",
		status: "Pending",
		dateCompleted: "04242022",
		userID: 1
	})
})
.then(res => res.json())
.then(data => console.log(data))


// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
//Patch method

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updating To Do List Item using Patch Method",
})
.then(res => res.json())
.then(data => console.log(data))


11. Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		status: "COMPLETED",
		dateCompleted: "021695",
})
.then(res => res.json())
.then(data => console.log(data))


12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
Delete a post

fetch('https://jsonplaceholder.typicode.com/todos/16', {
	method: 'DELETE',
})